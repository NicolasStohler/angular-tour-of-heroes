//import * as _ from "underscore"   // needs reference to .js file in systemjs.config.js
//import * as _ from 'lodash';        // needs reference to .js file in systemjs.config.js

export class Hero {
  constructor(public id: number,
              public name: string) { }

  private _secretName: string = "myUndef";

  get secretName(): string {
    // return this._secretName;
    if(this.name){
      return "The real " + this.name
        //+ _.isNumber("Hello TypeScript!", 20, "x");
        // + _.isUndefined(this);
      ;
    } else {
      return "<invisible>";
    }
  }
  set secretName(name : string) {
    this._secretName = name;
  }
}
