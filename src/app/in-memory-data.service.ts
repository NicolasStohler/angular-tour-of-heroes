import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    let heroes = [
      {id: 11, name: 'Mr. Nice'},
      {id: 12, name: 'Narco'},
      {id: 13, name: 'Bombasto'},
      {id: 14, name: 'Celeritas'},
      {id: 15, name: 'Magneta'},
      {id: 16, name: 'RubberMan'},
      {id: 17, name: 'Dynama'},
      {id: 18, name: 'Dr IQ'},
      {id: 19, name: 'Magma'},
      {id: 20, name: 'Tornado'}
    ];
    // let heroes = [
    //   new Hero(11,'Mr. Nice' ),
    //   new Hero(12, 'Narco' ),
    //   new Hero(13, 'Bombasto' ),
    //   new Hero(14, 'Celeritas' ),
    //   new Hero(15, 'Magneta' ),
    //   new Hero(16, 'RubberMan' ),
    //   new Hero(17, 'Dynama' ),
    //   new Hero(18, 'Dr IQ' ),
    //   new Hero(19, 'Magma' ),
    //   new Hero(20, 'Tornado' )
    // ];
    return {heroes};
  }
}
